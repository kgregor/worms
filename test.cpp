//#include <ilcplex/ilocplex.h>

#include <iostream>
#include <lemon/list_graph.h>
#include <lemon/euler.h>
#include <math.h>

//#include <size_problem.hxx>
//#include <include/graph_utils.hxx>
#include <graph_utils.hxx>

#include <lemon/core.h>
#include <lemon/concepts/graph.h>
#include <ilcplex/ilocplex.h>
#include <ilconcert/iloenv.h>

ILOSTLBEGIN

using namespace lemon;

using namespace std;

const int DISTANCE = 4;
const int HYPO = -1;
int T;

//class SolveSizesGraph{
//public:
//	int ncc_graph;
//
//
//	SolveSizesGraph(const MyGraph & graph)
//	{
//		assert(graph.isProbSet);
//		ListDigraph::NodeMap<int> compMap(graph);
//		ncc_graph=connectedComponents( graph,compMap);
//
//		std::cout << "CONGRATULATIONS YOU HAVE FOUND " << ncc_graph << " SUBGRAPHS "<< std::endl;
//
//	}
//
//};

float cost(Point a, Point b){
	return (a.x-b.x)*(a.x-b.x) + (a.y-b.y)*(a.y-b.y);
}

/*
 *	tells if two pixel lie in a small enough neighborhood
 */
bool closeEnough(Point a, Point b) {
	return (max(abs(a.x-b.x),abs(a.y-b.y))<DISTANCE);
}


int main() {
	std::cerr << "ehre" << std::endl;
	MyGraph g;

	string filename = "../testdata/single_encounter.h5.graph_fast";
	deserializeGraphFast(filename, "entire", g);
	std::map<int, std::vector<Point> > pos_map;
	deserializePositions(filename, "entire", pos_map);
	std::map<int, std::vector<float> > int_map;
	deserializeIntensities(filename, pos_map, int_map);



//				/*
//				 *  FOR TESTING PURPOSES
//				 */
//				//components
//				ListDigraph::Node c0 = g.addNode();
//				ListDigraph::Node c1 = g.addNode();
//				ListDigraph::Node c2 = g.addNode();
//				ListDigraph::Node c3 = g.addNode();
//				ListDigraph::Node c4 = g.addNode();
//				g.addArc(c0,c2);
//				g.addArc(c1,c2);
//				g.addArc(c2,c3);
//				g.addArc(c2,c4);
//				std::map<int, std::vector<Point> > pos_map;
//				std::vector<Point> & pos0=pos_map[0];
//				pos0.push_back(Point(0,1,1));
//				pos0.push_back(Point(0,2,1));
//				std::vector<Point> & pos1=pos_map[1];
//				pos1.push_back(Point(0,4,2));
//				pos1.push_back(Point(0,4,3));
//
//				std::vector<Point> & pos2=pos_map[2];
//				pos2.push_back(Point(1,2,1));
//				pos2.push_back(Point(1,3,1));
//				pos2.push_back(Point(1,3,2));
//
//				std::vector<Point> & pos3=pos_map[3];
//				pos3.push_back(Point(2,2,1));
//				pos3.push_back(Point(2,3,1));
//				std::vector<Point> & pos4=pos_map[4];
//				pos4.push_back(Point(2,5,1));
//				pos4.push_back(Point(2,5,2));
//
//				//////////////////////////////////////////
//
//				std::map<int, std::vector<float> > int_map;
//				std::vector<float> & int0=int_map[0];
//				int0.push_back(0.3);
//				int0.push_back(0.7);
//				std::vector<float> & int1=int_map[1];
//				int1.push_back(0.5);
//				int1.push_back(0.5);
//
//				std::vector<float> & int2=int_map[2];
//				int2.push_back(0.3);
//				int2.push_back(0.7);
//				int2.push_back(1.2);
//
//				std::vector<float> & int3=int_map[3];
//				int3.push_back(0.3);
//				int3.push_back(0.7);
//				std::vector<float> & int4=int_map[4];
//				int4.push_back(0.6);
//				int4.push_back(0.4);


	/*********************************************************************
	 *
	 * 			CONSTRUCT OUR GRAPH WITH META- & COLOR-NODES
	 *
	 *********************************************************************/

				//TODO
				IloEnv env;


	lemon::ListDigraph graph_pixel;
	lemon::ListDigraph::NodeMap<bool> cc_map(graph_pixel);
	lemon::ListDigraph::NodeMap<bool> meta_map(graph_pixel);
	lemon::ListDigraph::NodeMap<int> color_map(graph_pixel); //0=not a color, 1=red, 2=blue
	lemon::ListDigraph::NodeMap<bool> is_color_map(graph_pixel); //for testing purposes
	lemon::ListDigraph::NodeMap<Point> points_map(graph_pixel);
	lemon::ListDigraph::NodeMap<float> intensity_map(graph_pixel);
	lemon::ListDigraph::ArcMap<float> cost_map(graph_pixel);
	lemon::ListDigraph::ArcMap<IloNumVar> f_map(graph_pixel);
	lemon::ListDigraph::NodeMap<IloNumVar> m_map(graph_pixel);
	lemon::ListDigraph::ArcMap<bool> is_f_var(graph_pixel);
	lemon::ListDigraph::NodeMap<int> time_map(graph_pixel);



	//get all nodes from the original graph g and put it into graph_pixel
	//these are the components
	for (ListDigraph::NodeIt n(g); n != INVALID; ++n) {
		ListDigraph::Node x = graph_pixel.addNode();
		cc_map[x] = true;
		meta_map[x]=false;
		color_map[x]=0;
		is_color_map[x]=false;
	}

	//now get the outgoing arcs and create arcs between the components
	for (ListDigraph::NodeIt n(g); n != INVALID; ++n)
		for (ListDigraph::OutArcIt e(g, n); e != INVALID; ++e) {
			ListDigraph::Arc arc = graph_pixel.addArc(n, g.target(e));
			is_f_var[arc] = false;
		}

	//go through the components and create structure with meta and color nodes
	int i = 0;

	vector<ListDigraph::Node> cc_at_t_0;

	for (ListDigraph::NodeIt n(g); n != INVALID; ++n) {

		vector<float> & temp_intensity = int_map[int_map.size()-i-1];
		vector<Point> & temp_pos = pos_map[pos_map.size()-1-i];

		i++;

		std::cerr<<temp_pos.size()<<" = "<<temp_intensity.size()<<std::endl;

		for (int j = 0; j < temp_pos.size(); ++j) {

			//add meta node and save position in points_map
			ListDigraph::Node meta = graph_pixel.addNode();
			meta_map[meta] = true;
			points_map[meta] = temp_pos.at(j);


			intensity_map[meta] = temp_intensity.at(j);
			cc_map[meta]=false;
			color_map[meta]=0;
			is_color_map[meta]=false;

			ListDigraph::Arc arc = graph_pixel.addArc(n,meta);
			cost_map[arc]=0;
			is_f_var[arc] = false;

			//add two color nodes
			ListDigraph::Node red = graph_pixel.addNode();
			ListDigraph::Node blue = graph_pixel.addNode();
			color_map[red] = 1;
			color_map[blue] = 2;
			is_color_map[blue]=true;
			is_color_map[red]=true;
			meta_map[red]=false;
			meta_map[blue]=false;
			cc_map[red]=false;
			cc_map[blue]=false;
			m_map[red] = IloNumVar(env,0,1,IloNumVar::Float);
			m_map[blue] = IloNumVar(env,0,1,IloNumVar::Float);
			intensity_map[red] = temp_intensity.at(j);
			intensity_map[blue] = temp_intensity.at(j);

			//add arcs from meta to the color nodes and sets 0 cost
			ListDigraph::Arc arc1 = graph_pixel.addArc(meta, red);
			ListDigraph::Arc arc2 = graph_pixel.addArc(meta, blue);
			cost_map[arc1]=0; cost_map[arc2]=0;
			is_f_var[arc1]=false; is_f_var[arc2]=false;
		}
	}

	/**************************************************************
	 *
	 * 	     NOW CONSTRUCT ARCS BETWEEN COLOR-PIXELS
	 *
	 **************************************************************/

	 //GET COMPONENT SUBGRAPH
	FilterNodes<ListDigraph> comp_graph(graph_pixel, cc_map);
	FilterNodes<ListDigraph> meta_graph(graph_pixel, meta_map);
	FilterNodes<ListDigraph> color_graph(graph_pixel, is_color_map);

	int counter1=0; int counter2=0; int counter3=0; int counter4=0; int counter5=0; int counter6=0; int counter7=0;

	//go through the graph and connect pixels if in neighborhood
	//first through the components
	i = 0;
	for (FilterNodes<ListDigraph>::NodeIt n(comp_graph); n != INVALID; ++n) {

		counter1++;

		//this finds a component's neighbor components
		vector<ListDigraph::Node> neighbors;
		for (ListDigraph::OutArcIt e(graph_pixel, n); e != INVALID; ++e) {
			counter2++;
			ListDigraph::Node curr_node = graph_pixel.target(e);
			if (cc_map[curr_node] == true){
				neighbors.push_back(curr_node);
			}
		}

		//here we find a component's meta nodes
		for (ListDigraph::OutArcIt e(graph_pixel, n); e != INVALID; ++e) {
			ListDigraph::Node curr_node = graph_pixel.target(e);
			if(meta_map[curr_node]==true){
				ListDigraph::Node curr_neighbor;

				//now find the meta nodes of neighboring components
				//go through all neighbors of the component related to curr_node(=meta node)
				for (std::vector<ListDigraph::Node>::size_type i = 0; i!= neighbors.size(); i++) {
					curr_neighbor = neighbors[i];
					counter3++;

					//get neighbor's meta nodes
					for (ListDigraph::OutArcIt b(graph_pixel, curr_neighbor); b != INVALID; ++b) {
						ListDigraph::Node curr_neighbor_out = graph_pixel.target(b);
						counter4++;
						if (cc_map[curr_neighbor_out] == false) { //c_n_o is meta node!
							if (closeEnough(points_map[curr_node],points_map[curr_neighbor_out])) {
								//now loop over all possible colors
								for (ListDigraph::OutArcIt c(graph_pixel, curr_node); c != INVALID; ++c) {
									counter5++;
									for (ListDigraph::OutArcIt d(graph_pixel, curr_neighbor_out); d != INVALID; ++d) {
										counter6++;
										if(color_map[graph_pixel.target(c)]==color_map[graph_pixel.target(d)]){
											//create arc and set cost
											ListDigraph::Arc f = graph_pixel.addArc(graph_pixel.target(c),graph_pixel.target(d));
											cost_map[f]=cost(points_map[curr_node],points_map[curr_neighbor_out]);
											is_f_var[f] = true;
											f_map[f] = IloNumVar(env,0,1,IloNumVar::Float);
											counter7++;
										}

									}
								}
							}
						}
					}
				}
			}
		}
	}

	std::cout << counter1 << " " << counter2 << " " << counter3 << " " << counter4 << " " << counter5 << " " << counter6 << " " << counter7 << "BAAAAAAAAAAAAAM" << std::endl;


	/**************************************************************
	 *
	 * 	         		NOW CREATE LP IN CPLEX
	 *
	 **************************************************************/

	//FILTER OUT f-VARIABLES
	FilterArcs<ListDigraph> var_graph(graph_pixel, is_f_var);

	try{

		IloModel model = IloModel(env);
		IloNumVar y0 = IloNumVar(env,"aha");
		IloNumVar z = IloNumVar(env,"bla");

		IloExpr myExpr(env);

		/**************************************************************
		 *
		 * 	         		OBJECTIVE FUNCTION
		 *
		 **************************************************************/

		//traverse graph and add up all f's with their according cost
		int varcount=0;
		ListDigraph::Arc aaarrr;
		for (FilterArcs<ListDigraph>::ArcIt a(var_graph); a != INVALID; ++a) {
			myExpr += f_map[a] * cost_map[a];
			varcount++;
		}
		std::cerr << "VARCOUNT: " << varcount << std::endl;
		model.add(IloMinimize(env,myExpr));

		/**************************************************************
		 *
		 * 	         		CONSTRAINTS
		 *
		 **************************************************************/


		/*
		 * CONSTRAINT 1
		 */
		int num_out=0;
		int num_in=0;
		int constraint_count = 0;
		for (FilterNodes<ListDigraph>::NodeIt i(color_graph); i != INVALID; ++i) {
			IloExpr constr1(env);
			for (ListDigraph::OutArcIt a(graph_pixel, i); a != INVALID; ++a) {
				num_out++;
				constr1 += f_map[a];
			}

			//only add constraint, if there is actually a variable!! (example: last timestep, there is no flow going out!)
			if (num_out >0){
				model.add(constr1 == m_map[i]);
				constraint_count++;
			}

			num_out=0;
		}

		std::cerr << constraint_count << " equations of constraint 1" << std::endl;

		/*
		 * CONSTRAINT 2
		 */
		constraint_count=0;
		num_in=0;
		int var_constr =0;
		//go over all meta nodes (=pixels)
		for (FilterNodes<ListDigraph>::NodeIt m(meta_graph); m != INVALID; ++m) {
			IloExpr constr2(env);
			//get everything that goes into children of m!
			//for each child of m (which are color nodes)
			for(ListDigraph::OutArcIt a(graph_pixel,m); a != INVALID; ++a){

				ListDigraph::Node child = graph_pixel.target(a);

				//for every flow that goes into that child
				for(ListDigraph::InArcIt e(graph_pixel,child); e != INVALID; ++e){

					//only look at colors pointing to this child
					if(is_color_map[graph_pixel.source(e)] == true){
						constr2 += f_map[e];
						var_constr++;
					}

				}

			}

			//only add constraint, if there is actually a variable!! (example: last timestep, there is no flow going out!)
			if(var_constr>0){
				model.add(constr2 == intensity_map[m]);
				constraint_count++;
			}

			var_constr=0;
		}

		std::cerr << constraint_count << " equations of constraint 2" << std::endl;

		/*
		 * CONSTRAINT 3
		 */
		//sum over all meta nodes
		constraint_count=0;
		num_out = 0;
		for (FilterNodes<ListDigraph>::NodeIt i(meta_graph); i != INVALID; ++i) {
			IloExpr constr3(env);
			//sum over their outgoing edges (=colors)
			for (ListDigraph::OutArcIt a(graph_pixel, i); a != INVALID; ++a) {
				num_out++;
				constr3 += m_map[graph_pixel.target(a)];
			}

			//only add constraint, if there is actually a variable!! (example: last timestep, there is no flow going out!)
			if (num_out >0){
				model.add(constr3 == intensity_map[i]);
				constraint_count++;
			}

			num_out=0;
		}

		std::cerr << constraint_count << " equations of constraint 3" << std::endl;


		/*
		 * CONSTRAINT 4
		 */
		constraint_count=0;
		num_in=0;
		for (FilterNodes<ListDigraph>::NodeIt j(color_graph); j != INVALID; ++j) {
			IloExpr constr4(env);
			for (ListDigraph::InArcIt a(graph_pixel, j); a != INVALID; ++a) {

				//only arcs that come from colors
				if(is_color_map[graph_pixel.source(a)]==true){
						num_in++;
						constr4 += f_map[a];
				}

			}

			//only add constraint, if there is actually a variable!! (example: last timestep, there is no flow going out!)
			if(num_in >0 ){
				model.add(constr4 == m_map[j]);
				constraint_count++;
			}

			num_in =0;

		}

		std::cerr << constraint_count << " equations of constraint 4" << std::endl;

		/**************************************************************
		 *
		 * 	         			HYPOTHESES
		 *
		 **************************************************************/


		/*
		 * STARTING VALUES
		 */
		int n_in;
		int n_color=0;
//		for (FilterNodes<ListDigraph>::NodeIt comp(comp_graph); comp != INVALID; ++comp) {
//
//			//find component without incoming edges
//			n_in=0;
//			for (ListDigraph::InArcIt a(graph_pixel, comp); a != INVALID; ++a) {
//				n_in++;
//			}
//
//			if(n_in ==0){
//				n_color++;
//				//get meta nodes
//				for (ListDigraph::OutArcIt a(graph_pixel, comp); a != INVALID; ++a) {
//
//					ListDigraph::Node meta = graph_pixel.target(a);
//					//get its children
//					for (ListDigraph::OutArcIt e(graph_pixel, meta); e != INVALID; ++e) {
//						ListDigraph::Node child = graph_pixel.target(e);
//						if(color_map[child]==n_color){
//							std::cerr<<"hypo added: component nr " << graph_pixel.id(comp)<< ": color "<<n_color<< " with int. "<< intensity_map[meta]<<std::endl;
//
//							model.add(m_map[child]==intensity_map[meta]);
//						}
//					}
//				}
//			}
//		}

		/*
		 * HYPOTHESIS VALUES
		 */

		if(HYPO == 0){

			int n_out;
			n_color=0;
			for (FilterNodes<ListDigraph>::NodeIt comp(comp_graph); comp != INVALID; ++comp) {

				//find component without outgoing edges (except to meta nodes)
				n_out=0;
				for (ListDigraph::OutArcIt a(graph_pixel, comp); a != INVALID; ++a) {
					if(cc_map[graph_pixel.target(a)] == true)
						n_out++;
				}

				if(n_out ==0){
					n_color++;
					//get meta nodes
					for (ListDigraph::OutArcIt a(graph_pixel, comp); a != INVALID; ++a) {

						ListDigraph::Node meta = graph_pixel.target(a);
						//get its children
						for (ListDigraph::OutArcIt e(graph_pixel, meta); e != INVALID; ++e) {
							ListDigraph::Node child = graph_pixel.target(e);
							if(color_map[child]==n_color){
								std::cerr<<"hypo added: component nr " << graph_pixel.id(comp)<< ":  color : "<<n_color<< " with int. "<< intensity_map[meta]<<std::endl;

								model.add(m_map[child]==intensity_map[meta]);
							}
						}
					}
				}
			}

		} else if (HYPO == 1){

			int n_out;
			n_color=3;
			for (FilterNodes<ListDigraph>::NodeIt comp(comp_graph); comp != INVALID; ++comp) {

				//find component without outgoing edges (except to meta nodes)
				n_out=0;
				for (ListDigraph::OutArcIt a(graph_pixel, comp); a != INVALID; ++a) {
					if(cc_map[graph_pixel.target(a)] == true)
						n_out++;
				}

				if(n_out ==0){
					n_color--;
					//get meta nodes
					for (ListDigraph::OutArcIt a(graph_pixel, comp); a != INVALID; ++a) {

						ListDigraph::Node meta = graph_pixel.target(a);
						//get its children
						for (ListDigraph::OutArcIt e(graph_pixel, meta); e != INVALID; ++e) {
							ListDigraph::Node child = graph_pixel.target(e);
							if(color_map[child]==n_color){
								std::cerr<<"hypo added: component nr " << graph_pixel.id(comp)<< ":  color : "<<n_color<< " with int. "<< intensity_map[meta]<<std::endl;

								model.add(m_map[child]==intensity_map[meta]);
							}
						}
					}
				}
			}

		}



		IloCplex cplex(model);
		cplex.solve();




		/**************************************************************
		 **************************************************************
		 * 	         			PRINT RESULTS
		 **************************************************************
		 **************************************************************/





		cplex.out() << "Solution value = " << cplex.getObjValue() << endl;

		vigra::MultiArrayShape<3>::type shape(7,5,3);
		vigra::MultiArray<3, int> volume(shape);

		//get variable values
		for (FilterNodes<ListDigraph>::NodeIt meta(meta_graph); meta != INVALID; ++meta) {
			int value = 0;
			int t,x,y;
			for (ListDigraph::OutArcIt a(graph_pixel, meta); a != INVALID; ++a) {
				ListDigraph::Node child = graph_pixel.target(a);
				t = points_map[meta].t;
				x = points_map[meta].x;
				y = points_map[meta].y;

				double var_val = cplex.getValue(m_map[child]);
				if(round(255*var_val)>value){
					value = round(255*var_val);
					std::cerr<<"pixel value: "<<value<<std::endl;

				}

			}
			std::cerr << "t: "<<t<<", x: "<<x<<", y: "<<y<<std::endl;
			volume(t,x,y) = value;

		}
		vigra::VolumeExportInfo info("img/blabla",".jpg");
		vigra::exportVolume(volume,info);


	} catch (Exception& e){
		std::cerr<<"exception geworfen"<<std::endl;
	}


return 0;
}

