# README #

This was part of the larva tracking project which ended in the publication 
*Learning to disambiguate indistinguishable objects over time using weakly supervised structured learning, IEEE Conference on Computer Vision and Pattern Recognition 2014
*
**Problem:** A video of larvae that are crawling around, often over one another. Task is to track these indistinguishable larvae.

**Idea:** At the first frame, all larvae are separated, we "color" each with one color, create a graph from all colored pixels and run a multi-commodity-flow algorithm on it.

**Note:** Please note that this code was created in short time and there are no tests due to lack of time.