#ifndef GRAPH_UTILS_HXX
#define GRAPH_UTILS_HXX


#include <vigra/hdf5impex.hxx>
#include <vigra/multi_array.hxx>
#include <lemon/list_graph.h>
#include<string.h>
#include<math.h>
#include<vector>
#include <vigra/matrix.hxx>
#include <vigra/linear_algebra.hxx>

class MyGraph: public lemon::ListDigraph  {
public:
	int max_worms;
	double average_worm;
	double sigma;

	lemon::ListDigraph::NodeMap<float> sizes;
	lemon::ListDigraph::NodeMap<int>  times;

	bool isProbSet; //FIXME make it private




	MyGraph() :  sizes(*this), times(*this)
	{
		max_worms=10;
		average_worm=130;
		sigma=0.3;
		isProbSet=false;

	}

//	void setProbs()
//	{
//		for (ListDigraph::NodeIt n(*this); n != lemon::INVALID; ++n)
//			{
//				probs[n].resize(max_worms+1);
//				computeProb(probs[n],sizes[n]);
//			}
//
//		isProbSet=true;
//
//	}
//private:
//	void computeProb(std::vector<double> & vec,double size)
//	{
//		std::vector<double>::iterator it;
//		double count=0;
//		double nsize=size/average_worm;
//		double lambda=0.5/(sigma*sigma);
//		for(it=vec.begin();it!=vec.end();it++)
//		{
//			*it=(-lambda*(nsize-count)*(nsize -count));
//			++count;
//		}
//
//		double sum=0;
//		for(it=vec.begin();it!=vec.end();it++)
//			sum+=*it;
//
//		for(it=vec.begin();it!=vec.end();it++)
//					*it/=sum;
//
//	}

};


class Point{
public:
	int t,x,y;
	Point (int t_,int x_, int y_): t(t_),x(x_), y(y_) {};
	Point (): t(0),x(0), y(0) {};

	std::string toString(){

		std::stringstream sstm;
		sstm << "Point (" << x<<","<<y<<") at time step "<<t;
		return sstm.str();
	}

};

void deserializePositions(std::string filename ,std::string label, std::map<int,std::vector<Point> > & positions)
{
	//FIXME: ONLY VALID FOR ENTIRE
	std::string path_group="graph_fast/"+label+"/";
    std::string sizes_g=path_group+"positions";
	vigra::HDF5ImportInfo info_pos(filename.c_str(),sizes_g.c_str());
	vigra::MultiArrayShape<2>::type shape_pos(info_pos.shapeOfDimension(0),info_pos.shapeOfDimension(1));
	vigra::MultiArray<2, int> pos1(shape_pos);
	vigra::readHDF5(info_pos, pos1);

	vigra::MultiArrayShape<2>::type shape_pos_t(info_pos.shapeOfDimension(1),info_pos.shapeOfDimension(0));
	vigra::MultiArray<2, int> pos(shape_pos_t);
	vigra::transpose(pos1,pos);

	int i=0;
	int j=0;


	while (i<pos.shape(0))
	{
		std::vector<Point> & vec=positions[pos(i,0)];
		j=i+1;
		while (pos(j,0)!=-1)
		{
				int t,x,y;
				t=pos(j,0);
				x=pos(j,1);
				y=pos(j,2);
				vec.push_back(Point(t,x,y));
				++j;
		}
		i=j+1;
	}

}

void deserializeTrueSizes(std::string filename ,std::string label, std::map<int,std::vector<Point> > & true_sizes)
{
	//FIXME: ONLY VALID FOR ENTIRE
	std::string path_group="graph_fast/"+label+"/";
    std::string sizes_g=path_group+"true_sizes";
	vigra::HDF5ImportInfo info_pos(filename.c_str(),sizes_g.c_str());
	vigra::MultiArrayShape<2>::type shape_pos(info_pos.shapeOfDimension(0),info_pos.shapeOfDimension(1));
	vigra::MultiArray<2, int> pos1(shape_pos);
	vigra::readHDF5(info_pos, pos1);

	vigra::MultiArrayShape<2>::type shape_pos_t(info_pos.shapeOfDimension(1),info_pos.shapeOfDimension(0));
	vigra::MultiArray<2, int> pos(shape_pos_t);
	vigra::transpose(pos1,pos);

	int i=0;
	int j=0;


	while (i<pos.shape(0))
	{
		std::vector<Point> & vec=positions[pos(i,0)];
		j=i+1;
		while (pos(j,0)!=-1)
		{
				int t,x,y;
				t=pos(j,0);
				x=pos(j,1);
				y=pos(j,2);
				vec.push_back(Point(t,x,y));
				++j;
		}
		i=j+1;
	}

}

void deserializeIntensities(std::string filename, std::map<int,std::vector<Point> > & positions, std::map<int,std::vector<float> > & intensities)
{
	//FIXME: ONLY VALID FOR ENTIRE
    std::string size="volume/data";
	vigra::HDF5ImportInfo info(filename.c_str(),size.c_str());
	vigra::MultiArrayShape<5>::type shape(info.shapeOfDimension(0),info.shapeOfDimension(1),info.shapeOfDimension(2),info.shapeOfDimension(3),info.shapeOfDimension(4));
	vigra::MultiArray<5, float> vol(shape);
	vigra::readHDF5(info, vol);


	std::map<int,std::vector<Point> >::iterator it;

	//loop over all positions
	for(it=positions.begin();it!=positions.end();it++)
	{
		std::vector<Point> & temp = (*it).second;
		int i=(*it).first;
		std::vector<float> & itemp=intensities[i];

		std::vector<Point>::iterator iter;
		for(iter=temp.begin();iter!=temp.end();iter++)
		{
			int t=(*iter).t;
			int x=(*iter).x;
			int y=(*iter).y;
			float ival=vol(t,0,x,y,0);
			itemp.push_back(ival);

		}

	}

}

void deserializeGraphFast(const std::string filename,const std::string label,MyGraph & graph)
{




	 std::string path_group="graph_fast/"+label+"/";
     std::string sizes_g=path_group+"sizes";
	 vigra::HDF5ImportInfo info_size(filename.c_str(),sizes_g.c_str());
	 vigra::MultiArrayShape<1>::type shape_sizes(info_size.shapeOfDimension(0));
	 vigra::MultiArray<1, int> sizes(shape_sizes);
	 vigra::readHDF5(info_size, sizes);

     std::string times_g=path_group+"times";
	 vigra::HDF5ImportInfo info_times(filename.c_str(),times_g.c_str());
	 vigra::MultiArrayShape<1>::type shape_times(info_times.shapeOfDimension(0));
	 vigra::MultiArray<1, int> times(shape_times);
	 vigra::readHDF5(info_size, times);




	 std::string connect_g=path_group+"connectivity";
	 vigra::HDF5ImportInfo info_connect(filename.c_str(),connect_g.c_str());
	 vigra::MultiArrayShape<1>::type shape_connect(info_connect.shapeOfDimension(0));
	 vigra::MultiArray<1, int> connect(shape_connect);
	 vigra::readHDF5(info_connect, connect);
	 std::cerr << " THERE 3" <<std::endl;
	 int nnodes=sizes.shape(0);
	 for(int i=0;i<nnodes;i++)
	 {
	 	 lemon::ListDigraph::Node x=graph.addNode();
	 	 graph.sizes[x]=sizes(i);
	 	 graph.times[x]=times(i);
	 }





	 int i=0;
	 int j=0;
	 while (i<connect.shape(0))
		{
		 j=i+1;
		 while (connect(j)!=-1)
			 {
			 	 lemon::ListDigraph::Node x=graph.nodeFromId(connect(i));
			 	 lemon::ListDigraph::Node y=graph.nodeFromId(connect(j));
//std::cerr << " THERE "<< j <<" , "<< i <<" , connect (j) =" << connect(j) <<"connect(i) "<< connect(i) << " nnnodes= "<< nnodes  <<std::endl;
			 	 graph.addArc(x,y);
			 	 j=j+1;
			 }

		 i=j+1;
		}

	 //graph.setProbs();

}
#endif

